#ifndef TICTACTOE_H
#define TICTACTOE_H

class  TicTacToe
{
private:
	const int overallBoardCount = 9;
	const int winCount = 3;
	const static int arraySize = 3;
	int playerCharacter = 0;
public:
	TicTacToe();
	~TicTacToe();
	bool checkWinner(int a[arraySize][arraySize], int player);
	static int returnTwo();
	
};

#endif
