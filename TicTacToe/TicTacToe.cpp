// TicTacToe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "TicTacToe.h"

TicTacToe:: TicTacToe()
{
}

TicTacToe::~ TicTacToe()
{
}

bool TicTacToe::checkWinner(int a[TicTacToe::arraySize][TicTacToe::arraySize], int player)
{
	int hScore = 0;
	int vScore = 0;
	int dScore = 0;
	int antiDScore = 0;
	int playerCharacter = 0;
	int boardCount = 0;

	if (player == 1)
	{
		playerCharacter = 1;
	}
	else if (player == 2)
	{
		playerCharacter = 2;
	}

	//checks rows and columns for horizontal and vertical wins
	for (int i = 0; i < arraySize; i++)
	{
		for (int j = 0; j < arraySize; j++)
		{
			boardCount++;
			if (a[i][j] == playerCharacter)
				hScore++;
			if (hScore == winCount)
			{
				std::cout << "Winner on the horizonal axis  for player : " << playerCharacter <<" at count of : " << boardCount << std::endl;
				return true;
			}
			if (a[j][i] == playerCharacter)
				vScore++;
			if (vScore == winCount)
			{
				std::cout << "Winner on the vertical axis for player : " << playerCharacter<< " at count of : " << boardCount << std::endl;
				return true;
			}
		}
		vScore = 0;
		hScore = 0;
	}

	//front diagonal check
	for (int i = 0; i < arraySize; i++)
	{
		for (int j = 0; j < arraySize; j++)
		{
			if (i == j && a[i][j] == playerCharacter)
				dScore++;
			if (dScore == winCount)
			{
				std::cout << "Winner on the front diagonal axis for player : " << playerCharacter << " at count of : " << boardCount << std::endl;
				dScore = 0;
				return true;
			}
		}
	}

	//back diagonal check
	for (int i = 0; i < arraySize; i++)
	{
		for (int j = 0; j < arraySize; j++)
		{
			if (i + j == (arraySize - 1) && a[i][j] == playerCharacter)
				antiDScore++;
			if (antiDScore == winCount)
			{
				std::cout << "Winner on the anti diagonal axis for player : " << playerCharacter << " at count of : " << boardCount << std::endl;
				antiDScore = 0;
				return true;
			}
		}
	}

	if (boardCount == overallBoardCount)
		std::cout << "There is a draw" << std::endl;
	std::cout << "No One Won" << std::endl;
	boardCount = 0;
	return false;
}
int TicTacToe::returnTwo()
{
	return 2;
}

int main()
{
	TicTacToe ttt;
	const int size = 3;

	int TicTacToeBoard[size][size] =
	{	1,0,0,
		1,1,1,
		0,0,1
	};
	
	//Used for testing
	/*
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			std::cout << TicTacToeBoard[i][j] << ' ';
		}
		std::cout << std::endl;
	}
	*/

	ttt.checkWinner(TicTacToeBoard, 1);

	system("Pause");

    return 0;
}