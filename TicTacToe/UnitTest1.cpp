#include "stdafx.h"
#include "CppUnitTest.h"
#include "TicTacToe.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

const int size = 3;
TicTacToe tttTest;

namespace UnitTest
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestMethodCheckWinnerHorizontal)
		{
			//checks there is a  horizontal winner
			int TicTacToeBoard[size][size] =
			{ 1,1,1,
				0,0,0,
				0,0,1
			};

			bool test = tttTest.checkWinner(TicTacToeBoard, 1);

			Assert::IsTrue(test);
		}
		TEST_METHOD(TestMethodCheckWinnerVertical)
		{
			//checks there is a vertical winner
			int TicTacToeBoard[size][size] =
			{ 1,0,0,
				1,0,0,
				1,0,1
			};

			bool test = tttTest.checkWinner(TicTacToeBoard, 1);

			Assert::IsTrue(test);
		}
		TEST_METHOD(TestMethodCheckWinnerDiagonal)
		{
			//checks there is a diagonal winner
			int TicTacToeBoard[size][size] =
			{ 1,0,0,
				0,1,0,
				0,0,1
			};

			bool test = tttTest.checkWinner(TicTacToeBoard, 1);

			Assert::IsTrue(test);
		}
		TEST_METHOD(TestMethodCheckWinnerAntiDiagonal)
		{
			//checks there is an antidiagonal winner
			int TicTacToeBoard[size][size] =
			{ 0,0,1,
				0,1,0,
				1,0,0
			};

			bool test = tttTest.checkWinner(TicTacToeBoard, 1);

			Assert::IsTrue(test);
		}
		TEST_METHOD(TestMethodCheckWinnerNoWinner)
		{
			//checks there is no valid winner 
			int TicTacToeBoard[size][size] =
			{ 0,0,0,
				0,0,0,
				0,0,0
			};

			bool test = tttTest.checkWinner(TicTacToeBoard, 1);

			Assert::IsFalse(test);
		}
	};
}